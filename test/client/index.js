const grpc = require('grpc')
const WeatherService = require('../../src/grpc/interface.js')

const client = new WeatherService('localhost:9090', grpc.credentials.createInsecure())

const getWeather = ({ city }) => {
  return new Promise((resolve, reject) => {
    client.getWeather({ city }, (error, response) => {
      if (error) {
        reject(error)
      } else {
        resolve(response)
      }
    })
  })
}

module.exports = { client, getWeather }
