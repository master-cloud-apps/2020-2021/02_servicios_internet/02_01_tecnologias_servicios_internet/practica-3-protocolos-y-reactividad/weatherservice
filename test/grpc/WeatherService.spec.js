const { expect } = require('chai')
const server = require('../../src/server')
const { getWeather } = require('../client/index.js')

describe('Testing the server', () => {
  beforeEach(() => server.start())
  afterEach(() => server.forceShutdown())
  it('Should get Rainy for Alava', () => {
    return getWeather({ city: 'Alava' })
      .then(response => expect(response.weather).to.be.equal('Rainy'))
  }).timeout(3200)
})
