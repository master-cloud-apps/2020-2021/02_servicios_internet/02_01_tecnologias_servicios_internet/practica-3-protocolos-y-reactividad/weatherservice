const getFirstLetter = (value) => value.charAt(0)
const isVowel = (char) => /[AaEeIiOoUu]/.test(char)
const getProcessTime = () => Math.ceil(Math.random() * 3000)
const resolveAfterTimeout = (valueToResolve) => {
  return new Promise((resolve) => {
    setTimeout(() => resolve(valueToResolve), getProcessTime())
  })
}

const getCityWeather = ({ city }) => {
  if (isVowel(getFirstLetter(city))) {
    return resolveAfterTimeout('Rainy')
  }
  return resolveAfterTimeout('Sunny')
}

module.exports = getCityWeather
