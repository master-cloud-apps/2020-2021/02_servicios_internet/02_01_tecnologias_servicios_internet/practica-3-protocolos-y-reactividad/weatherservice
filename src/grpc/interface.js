const grpc = require('grpc')
const protoLoader = require('@grpc/proto-loader')
const path = require('path')

const packageDefinition = protoLoader.loadSync(path.join(__dirname, '..', '..', 'weather.proto'),
  {
    keepCase: true,
    longs: String,
    enums: String,
    defaults: true,
    oneofs: true
  })

const weatherServiceProto = grpc.loadPackageDefinition(packageDefinition)

module.exports = weatherServiceProto.org.eyo.grpc.WeatherService
