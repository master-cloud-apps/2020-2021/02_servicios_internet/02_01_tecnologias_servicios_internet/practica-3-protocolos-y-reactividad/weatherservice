const getCityWeather = require('../services')

const GetWeather = (call, callback) => {
  console.log(`Request received: ${JSON.stringify(call.request)}`)
  const { city } = call.request
  getCityWeather({ city })
    .then(weather => callback(null, { city, weather }))
}

module.exports = { GetWeather }
