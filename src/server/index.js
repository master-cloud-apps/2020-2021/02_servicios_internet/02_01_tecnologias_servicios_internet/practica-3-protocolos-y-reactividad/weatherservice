const grpc = require('grpc')
const WeatherService = require('./../grpc/interface.js')
const WeatherServiceImpl = require('./../grpc/WeatherService')

const server = new grpc.Server()
server.addService(WeatherService.service, WeatherServiceImpl)
server.bind('127.0.0.1:9090', grpc.ServerCredentials.createInsecure())
console.log('gRPC server running at http://127.0.0.1:9090')

module.exports = server
