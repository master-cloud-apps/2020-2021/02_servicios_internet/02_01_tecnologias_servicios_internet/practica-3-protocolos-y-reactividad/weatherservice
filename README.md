# WeatherService

Devuelve información metereológica.

## Detalles de implementación

* Ofrecerá una API gRPC en la que recibirá una ciudad como parámetro y devolverá Rainy o Sunny.
* Se implementará en Node.
* Para simular el cálculo interno devolverá “Rainy” si la ciudad empieza por vocal y “Sunny” si empieza por consonante.
* Simulará un tiempo de proceso de 1 a 3 segundos aleatorio con un setTimeout.
* El formato de la API gRPC será:
  ````
  service WeatherService {
    rpc GetWeather(GetWeatherRequest) returns (Weather);
  }
  message GetWeatherRequest { 
    string city = 1;
  }
  message Weather { 
    string city = 1; 
    string weather = 2;
  }
  ````
